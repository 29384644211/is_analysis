# 流程图样例

```mermaid
graph TD;
    客户录单-->审核;
    审核-->C;
    审核-->D;
    C-->D;
```

## plantuml

```plantuml
@startuml
class Object << general >>
Object <|--- ArrayList

note top of Object : In java, every class\nextends this one.

note "This is a floating note" as N1
note "This note is connected\nto several objects." as N2
Object .. N2
N2 .. ArrayList

class Foo
note left: On last defined class

@enduml
```

## 取款用例

|项目|属性|
|----|-----|
|用例名称|取款|
|参与者|银行卡用户|
|主要参与者|主银行系统（次要参与者）|
|目标|用户使用银行卡从ATM机获取现金|
|范围|银行ATM系统|
|前置条件|用户将银行卡插入ATM|
|后置事件|交易日志被保存|

- 主事件流

  - ATM系统识别卡的ID和账号，并用主银行系统验证其有效性
  - 用户输入密码，ATM系统验证其有效性
  - 用户选择取款，并输入提取金额，该数额必须在100~3000之间，100的倍数
    - 如果**余额不足**，拒绝取款
  - ATM系统通知账户所在的主银行系统，传递账号和取款金额，并接受返回的确认信息和账户余额
  - ATM系统发放现金、卡，并打印收据
  - ATM将事务记入日志
- 业务规则
  - Aa
  - Baa
- [参考资料](README.md#)

